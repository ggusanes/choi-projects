<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" >
  <fieldset>
    <div class="container"><?php
      require('fields.php');
      input_field($errors, 'name', 'Name');
      input_field($errors, 'email', 'Email');
      input_field($errors, 'address', 'Address');?>

    <div class="field">
    <label><b>Date Of Birth:</b></label>
    <input type="date" name="dob" required>
    </div>

    <div class="field">
    <label for="pass">Password:</label>
    <input name="passwd" type="password" id="pass"/>
    </div>

    <div class="field">
    <label>Gender:</label>
    Male <input name="gender" type="radio" <?php if(isset($_POST['gender']) && $_POST['gender']=="male") echo "checked";?> id="male" value="male"/>
    Female <input name="gender" type="radio" <?php if(isset($_POST['gender']) && $_POST['gender']=="female") echo "checked";?> id="female" value="female"/>
    <span class="error"><?php if(isset($errors['gender'])) echo $errors['gender'];?></span>
    </div>

    <input name="register" type="submit" value="Register"/>
    <div>
  </fieldset>
</form>
