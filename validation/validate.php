<?php
	function validateEmail(&$errors, $field_list, $field_name){ // ***,POST['var']

		$pattern = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/';
		if (!isset($field_list[$field_name]) || empty($field_list[$field_name])){
				$errors[$field_name] = "Email is required";
		}else{
				$trustedVar = test_input($field_list[$field_name]);
				if (!preg_match($pattern,$trustedVar)){
					$errors[$field_name] = "Invalid email format";
				}
		}
	}

	function validateName(&$errors, $field_list, $field_name){

		$pattern = "/^[a-zA-Z ]*$/";
		if (!isset($field_list[$field_name]) || empty($field_list[$field_name])) {
			$errors[$field_name] = "Name is required";
		} else {
			//$name = test_input($_POST["name"]);
			$trustedVar = test_input($field_list[$field_name]);
			// check if name only contains letters and whitespace
				if (!preg_match($pattern,$trustedVar)) {
				$errors[$field_name] = "Only letters and white space allowed";
			}
		}
	}

	function validatePattern(&$errors, $field_list, $field_name,$pattern){
		if (!isset($field_list[$field_name]) || empty($field_list[$field_name])) {
			$errors[$field_name] = "Required";
		} else {
			$trustedVar = test_input($field_list[$field_name]);
			// check if name only contains letters and whitespace
				if (!preg_match($pattern,$trustedVar)) {
				$errors[$field_name] = "Invalid";
			}
		}
	}

	function validateGender(&$errors, $field_list, $field_name){
		if (!isset($field_list[$field_name])) {
			$errors[$field_name] = "Gender is required";
		}
	}

	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
?>
