<?php
session_start();
require 'modules/loginUser.php';
if(isset($_SESSION['loggedIn'])){
  header("Location: search.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link href="css/loginstyle.css" rel="stylesheet" type="text/css"/>
    <script src="script.js"></script>
  </head>

  <body>
	<div id="wrapper"><?php
    include "basicFrame/header.php";
    include "basicFrame/menu.php";?>
		<div id="contents">
			<h1>Log In</h1>
			<form method="post" action="login.php">
				<div class="container">
					<label><b>Email:</b></label>
					<input type="email" placeholder="Enter Email" name="email" required>

					<label><b>Password:</b></label>
					<input type="password" placeholder="Enter Password" name="password" required>
					<span class="login_error"><?php if(isset($loginError)) echo $loginError; ?></span>

					<button type="submit" name="loginBtn">Login</button>
				</div>
			</form>
			<form action="user_reg.php">
				<div class="container2">
					<button>Sign Up</button>
				</div>
			</form>
		</div>
		<?php include "basicFrame/footer.php"; ?>
	</div>
  </body>
</html>
