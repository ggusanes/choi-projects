<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Search</title>
	<link href="css/searchStyle.css" rel="stylesheet" type="text/css"/>  <!-- change css file from mystyle2 to mystyle3 -->
	<script type='text/javascript' src="pages/searchPage.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaYuOl3Lib84uVvo5zt797eJ12p--rkx8&callback=myMap"></script>
</head>
<body>
  <div id="wrapper"><?php
			include "basicFrame/header.php";
			include "basicFrame/menu.php";?>
		<div id="contents">
			<h1>Search for Parks</h1>
			<div class="bySuburb">
				<?php
					include '/pages/funcsForIndividualItem.php';
					require 'modules/db.php';

					$dbParks = new DBData();
					$results = $dbParks->DBFuncLimited("dataparks","Suburb","Suburb","");	?>

				<select name="suburbs" onchange="getSuburbs(this.value)">
					<option value="">Select a suburb</option>
					<?php
					for($i=0;$i<$dbParks->getRowNum();$i++) {
						$sub =  $results['Suburb']; ?>
					  <option value=<?php echo "'".$sub."'"; ?>><?php echo $sub ?></option>
					  <?php
					  $results = $dbParks->fetch();
					} ?>
				</select>
			</div>
			<div class="byName">
				<input type="search" id="typeSubName" placeholder="Type a name!"> <button onclick="searchByName()">Search!</button>
			</div>

			<div class="byRating">
				Search By Rating:
				|<input type="radio" name="rating" value=1 onchange="searchByRating(this.value)"> More than 1
    		|<input type="radio" name="rating" value=2 onchange="searchByRating(this.value)"> More than 2
    		|<input type="radio" name="rating" value=3 onchange="searchByRating(this.value)"> More than 3
				|<input type="radio" name="rating" value=4 onchange="searchByRating(this.value)"> More than 4
				|<input type="radio" name="rating" value=5 onchange="searchByRating(this.value)"> 5 |
			</div>

			<div class="byUserDistance">
				<button onclick="nearestPlace()">Find the nearest park space</button>
			</div>
			<div class="showResults">
				<div id="googleMap1"></div>
			</div>
		</div> <!--end contents(id) -->
		<?php include "basicFrame/footer.php"; ?>
	</div> <!--end wrapper(id) -->
</body>
</html>
