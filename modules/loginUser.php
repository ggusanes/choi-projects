<?php
require 'modules/db.php';

if (isset($_POST['loginBtn'])) {
	//Creating variables
	$email=$_POST['email'];
	$password=$_POST['password'];

	//Hashing password
	$password = md5($password);

	//Prepare SQL query
	$sql="SELECT * FROM members WHERE email = :email AND password= :password";
	$records=$conn->prepare($sql);

	//Prevent SQL Injection
	$records->bindValue(':email', $email);
	$records->bindValue(':password', $password);

	//Execute query and fetch results
	$records->execute();
	$results = $records->fetch(PDO::FETCH_ASSOC);

	//Log user in if correct email / password provided
	if ($results > 0) {
		$_SESSION['name'] = $results['name'];
		$_SESSION['userID'] = $results['userID'];
		$_SESSION['loggedIn'] = true;
		header("Location:search.php");
	} else {
		$loginError = "Email / Password combination incorrect";
	}
}
?>
