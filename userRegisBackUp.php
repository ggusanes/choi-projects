<?php
session_start();
require 'modules/registerUser.php';
if(isset($_SESSION['loggedIn'])){
  header("Location: search.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>User Registration</title>
    <link href="css/loginstyle.css" rel="stylesheet" type="text/css">
	  <style> <?php include 'css/loginstyle.css'; ?> </style>
    <script type="text/javascript" src="pages/userReg.js"></script>
  </head>
  <body>
	<div id="wrapper"><?php
  			include "basicFrame/header.php";
  			include "basicFrame/menu.php";?>
		<div id="contents">
			<h1>Create an Account</h1>

			<form method="post" action="user_reg.php">
				<div class="container">
					<label><b>Name:</b></label>
					<input type="text" pattern="[a-zA-Z ]+" placeholder="Enter Your Full Name" name="name" value="<?php if(isset($name)) echo $name ?>" title="Only enter letters and spaces" required>

					<label><b>Email:</b></label>
					<input type="email" placeholder="Enter a valid email address" name="email" value="<?php if(isset($email)) echo $email ?>" required>
					<span class="email_error"><?php if(isset($emailError)) echo $emailError; ?></span>

					<label><b>Date Of Birth:</b></label>
					<input type="date" name="dob" required>

					<label><b>Postcode:</b></label>
					<input type="text" pattern="[0-9]{4}" placeholder="Enter a valid postcode" name="postcode" value="<?php if(isset($postcode)) echo $postcode ?>" title="Enter a number between 0000 and 9999" required>

					<label><b>Password:</b></label>
					<input type="password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$" placeholder="Enter Password" name="password" id="password1"
						   title="Must contain at least 1 number, contain both lowercase and uppercase letters and be atleast 8 characters long" required>

					<label><b>Confirm Password:</b></label>
					<input type="password" placeholder="Confirm Password" name="password2" id="password2" required>

					<p>I accept the <u>Terms and Conditions</u> <input type="checkbox" name="terms" required></p>
					<button type="submit" name="regBtn">Create Account</button>

				</div>
			</form>
		</div>
		<?php include "basicFrame/footer.php"; ?>
	</div>
  </body>
</html>
