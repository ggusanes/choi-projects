<?php
session_start();
require 'modules/db.php';

if(isset($_GET['itemNum'])){
  $parkId = $_GET['itemNum'];
}
if(isset($parkId) && !empty($_POST['reviewContents']) && isset($_POST['userRating']) && isset($_SESSION['userID']) ){

  $reviewContents = $_POST['reviewContents'];
  $userRating = $_POST['userRating'];
  $currentDate = date("Y-m-d");
  $parkID = $_GET['itemNum'];
  $userID = $_SESSION['userID'];

  $sql ="INSERT INTO reviews(parkID,userID,uploadedDate,contents,rating) VALUES ('$parkID','$userID','$currentDate','$reviewContents','$userRating')";
  $result=$pdo->query($sql);
}?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Item Page</title>
    <link href="css/samplepagestyle.css" rel="stylesheet" type="text/css"/>
    <script type='text/javascript' src="pages/searchPage.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaYuOl3Lib84uVvo5zt797eJ12p--rkx8&callback=myMap"></script>
  </head>
  <body>
    <?php include '/pages/funcsForIndividualItem.php';
      $dbParks = new DBData();
      $dbReviews = new DBData();
      $resultsParks = $dbParks->DBFunc("dataparks","parkID",$parkId);
      $resultsItem = $dbReviews->DBFunc("reviews","parkID",$parkId); ?>
	<div id="wrapper"><?php
  			include "basicFrame/header.php";
  			include "basicFrame/menu.php"; ?>
		<div id="title"><b><?php echo $resultsParks['Name'];?></b></div>
		<div id="content">
			<label><b>User Reviews</b></label> <?php
      $sql="SELECT reviewID,parkID,reviews.userID,uploadedDate,contents,rating,name FROM reviews JOIN (SELECT userID,name FROM members) AS NT ON reviews.userID=NT.userID AND parkID=:parkID";
      $records=$pdo->prepare($sql);
      $records->bindParam(':parkID',$_GET['itemNum']);
      $records->execute();
      $rowNum = $records->rowCount();
      $results = $records ->fetch(PDO::FETCH_ASSOC);

      if($rowNum != 0){
        for($i=0;$i<$rowNum;$i++){ ?>
          <div class="Box">
            <h2> <?php
                if($results == 0){
                  echo "No records!";
                }else{
                  echo $results['name']." / ".$results['uploadedDate']." / ".$results["rating"]." Stars";
                }?>
            </h2>
            <p><?php
                if($results == 0){
                  echo "No records!";
                }else{
                  ?><?php echo $results['contents'];?><?php
                }?>
            </p>
            <?php $results = $records ->fetch(PDO::FETCH_ASSOC);?>
          </div><?php
        } // end for
      }else{ ?>
        <div class="Box">
          <p>No reviews!</p>
        </div> <?php
      }?>

      <div class="addReviews"> <?php
          if (isset($_SESSION['loggedIn'])){?>
            Leave your review and estimate this park from 1 to 5!<?php //echo $_SESSION['userID']; ?>
            <form name="usersReviews" action="individualitempage.php?itemNum=<?php echo $parkId ?>" method="post">
              <input type="textarea" id="reviewContents" name="reviewContents" placeholder="Enter your review!">
              <select name="userRating" id="userRating">
                <option value="">Select</option>
                <option value=1>1</option>
                <option value=2>2</option>
                <option value=3>3</option>
                <option value=4>4</option>
                <option value=5>5</option>
              </select>
              <input type="submit" value="Submit">
            </form><?php
          }else{?>
            <p>If you want to leave a review, Please <a href="login.php">Log in!</a></p><?php
          }?>
      </div>

		</div> <!--end div content-->
		<div id="content2">
      <div id="googleMap">
        <script>var parkID = <?php echo $parkId; ?> </script>
        <p>Show a google map below!<script src="pages/individualItem.js"></script></p>
      </div>
      <label><b>Park Info</b></label>
			<div class="Box">
        <p>Name: <?php echo $resultsParks['Name'];?></p>
        <p>Suburb: <?php echo $resultsParks['Suburb'];?></p>
        <p>Average Rating: <?php
                                $avg = $dbReviews->getAvgRating("reviews","parkID",$parkId);
                                if($avg['avgRating']== NULL){
                                  echo "No records";
                                }else{
                                  echo $avg['avgRating'];
                                }?>
        </p>
      </div>
		</div> <!--end content2 -->
		<?php include "basicFrame/footer.php"; ?>
	</div>
  </body>
</html>
