<?php
session_start();
require 'modules/registerUser.php';
if(isset($_SESSION['loggedIn'])){
  header("Location: search.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>User Registration</title>
    <link href="css/loginstyle.css" rel="stylesheet" type="text/css">
	  <style> <?php include 'css/loginstyle.css'; ?> </style>
    <script type="text/javascript" src="pages/userReg.js"></script>
  </head>
  <body>
	<div id="wrapper"><?php
  			include "basicFrame/header.php";
  			include "basicFrame/menu.php";?>
		<div id="contents">
			<h1>Create an Account</h1>
      <?php
      	$errors = array();
      	if ($_SERVER["REQUEST_METHOD"] == "POST") {
        		require '/validation/validate.php';
            validatePattern($errors,$_POST,'name',"/^[a-zA-Z ]*$/");
            validatePattern($errors,$_POST,'email',"/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/");
            validateGender($errors,$_POST,'gender');

        		if (count($errors) > 0) {
          			// redisplay the form
          			include '/validation/regisForm.php';
        		} else {
        			echo 'form submitted successfully with no errors';
              header("Location: search.php");
        		}
      	}else {
      		include '/validation/regisForm.php';
      	}?>
		</div>
		<?php include "basicFrame/footer.php"; ?>
	</div>
  </body>
</html>
