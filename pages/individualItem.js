var locations=[[],[]];
locations = getDataFromDB(parkID,3);
var locX = locations[0][4];
var locY = locations[0][5];

var mapCanvas = document.getElementById("googleMap");
mapCanvas.style = "width:350px;height:200px";
var myCenter = new google.maps.LatLng(locX,locY);
var mapOptions = {center: myCenter, zoom: 14};
var map = new google.maps.Map(mapCanvas,mapOptions);
var marker = new google.maps.Marker({
  position: myCenter,
  animation: google.maps.Animation.BOUNCE
});
marker.setMap(map);
