<?php
// first inner-array meanes id
// second inner-array points to a certain suburb's sub-data
$json = array();

if(isset($_POST['dataSet'])){
  try{

    require '../modules/db.php';
    $records;$rowNum;

    if($_POST['idx'] == 0){
      // idx '0' represents 'search name'
      $sql="SELECT * FROM dataparks WHERE name=:name";
      $records=$pdo->prepare($sql);
      $records->bindParam(':name',$_POST['dataSet']);
      $records->execute();
      $rowNum = $records->rowCount();

    }else if($_POST['idx'] == 1){

      $sql="SELECT * FROM dataparks WHERE suburb=:suburb";
      $records=$pdo->prepare($sql);
      $records->bindParam(':suburb',$_POST['dataSet']);
      $records->execute();
      $rowNum = $records->rowCount();

    }else if($_POST['idx'] == 2){

      $sql="SELECT Name,Suburb,ParkCode,Street,Latitude,Longitude,NT.parkID AS parkID FROM dataparks JOIN (SELECT parkID,AVG(rating) AS AvgRating FROM reviews GROUP BY parkID HAVING AvgRating>=:rating) AS NT ON dataparks.parkID=NT.parkID";
      //$sql="SELECT * FROM dataparks";
      $records=$pdo->prepare($sql);
      $records->bindParam(':rating',$_POST['dataSet']);
      $records->execute();
      $rowNum = $records->rowCount();
    }else if($_POST['idx'] == 3){

      $sql="SELECT * FROM dataparks WHERE parkID=:id";
      $records=$pdo->prepare($sql);
      $records->bindParam(':id',$_POST['dataSet']);
      $records->execute();
      $rowNum = $records->rowCount();
    }else{
      // this represents whole dataparks
      // idx '1' represents 'search suburb'
      $sql="SELECT * FROM dataparks";
      $records=$pdo->prepare($sql);
      $records->execute();
      $rowNum = $records->rowCount();
    } // end else

    for($i=0;$i<$rowNum;$i++){
      $results = $records ->fetch(PDO::FETCH_ASSOC);
      $json[$i][0] = $results['Name'];
      $json[$i][1] = $results['Suburb'];
      $json[$i][2] = $results['ParkCode'];
      $json[$i][3] = $results['Street'];
      $json[$i][4] = $results['Latitude'];
      $json[$i][5] = $results['Longitude'];
      $json[$i][6] = $results['parkID'];
    } // end for
  }catch(PDOException $e){
    echo $e->getMessage();
  }
}
echo json_encode($json);
?>
