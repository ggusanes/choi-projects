<?php
class DBData{
  var $rowNum=0;
  var $results;
  var $records;

  function getRowNum(){
    return $this->rowNum;
  }

  function fetch(){
    $this->results =  $this->records->fetch(PDO::FETCH_ASSOC);
    return $this->results;
  }

  function getAvgRating($tblName,$colName,$id){
    require 'modules/db.php';
    $sql="SELECT AVG(rating) AS avgRating FROM $tblName WHERE $colName=:id";
    $records=$pdo->prepare($sql);
    $records->bindParam(':id',$id);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $rowNum = $records->rowCount();
    return $results;
  }

  function DBFunc($tblName,$colName,$search){
    require 'modules/db.php';
    $sql="SELECT * FROM $tblName"; // WHERE $colName=:search";
    if($search != ""){
      $sql = $sql." WHERE $colName=:search";
    }
    $records=$pdo->prepare($sql);
    $records->bindParam(':search',$search);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $this->records = $records;
    $rowNum = $records->rowCount();
    $this->rowNum=$rowNum;
    if($rowNum == 0){
      return 0;
    }else{
      return $results;
    } // end else
  }//end function

  function DBFuncLimited($tblName,$selected,$colName,$search){
    require 'modules/db.php';
    $sql="SELECT DISTINCT $selected FROM $tblName"; // WHERE $colName=:search";
    if($search != ""){
      $sql = $sql." WHERE $colName=:search";
    }
    $records=$pdo->prepare($sql);
    $records->bindParam(':search',$search);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $this->records = $records;
    $rowNum = $records->rowCount();
    $this->rowNum=$rowNum;
    if($rowNum == 0){
      return 0;
    }else{
      return $results;
    } // end else
  } // end function
} // end class
?>
