function getDataFromDB(dataSet,idx){
  //Get data from DB by using Ajax
  var locations=[[],[]];
  $.ajax({
   url:'pages/connDbBySub.php',
   type:'post',
   dataType:'json',
   data: {dataSet:dataSet,idx:idx},
   async: false,
   success:function(data){
     locations = data;
     //Test : alert("The number of rows is "+data.length);
   }
 });
 return locations;
}

function makeTable(dataSet,idx){
  var NUMTBLCOLS = 6;
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  table.setAttribute("id","table1");
  // Fill titles in first rows and  their columns
  var firstRow = table.insertRow(0);
  firstRow.insertCell(0).innerHTML="Name";
  firstRow.insertCell(1).innerHTML="Suburb";
  firstRow.insertCell(2).innerHTML="Park Code";
  firstRow.insertCell(3).innerHTML="Street Name";
  firstRow.insertCell(4).innerHTML="Latitude";
  firstRow.insertCell(5).innerHTML="Longitude";
  //Get data from DB by using Ajax
  var locations=[[],[]];
  locations = getDataFromDB(dataSet,idx);

  if(locations.length != 0 ){
    for(var i=0;i<locations.length;i++){
      // check if passed data (parameter) matches data of the set.
        var nextRow = table.insertRow(-1);
        for(var j=0;j<NUMTBLCOLS;j++){
          //make a new cell
          var newCell = nextRow.insertCell(-1);
          if(j==0){ // only make a link on a first column
            var content = locations[i][j];
            var a = document.createElement('a');
            var linkText = document.createTextNode(content);
            var itemInfo = locations[i][6];
            a.appendChild(linkText);
            a.href = 'individualitempage.php?itemNum='+itemInfo;
            newCell.appendChild(a);
          }else{
            newCell.innerHTML = locations[i][j];
          }
        }   // end for
    }   // end for
    pos1.appendChild(table);
  }
  return locations;
} // end function

function makeTable2(locations,locationsIdx){
  var MAXNUMROWS=5;
  var NUMTBLCOLS=6;
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  table.setAttribute("id","table1");
  // Fill titles in first rows and  their columns
  var firstRow = table.insertRow(0);
  firstRow.insertCell(0).innerHTML="Name";
  firstRow.insertCell(1).innerHTML="Suburb";
  firstRow.insertCell(2).innerHTML="Park Code";
  firstRow.insertCell(3).innerHTML="Street Name";
  firstRow.insertCell(4).innerHTML="Latitude";
  firstRow.insertCell(5).innerHTML="Longitude";

  for(var i=0;i<MAXNUMROWS;i++){
    // make a new row
    var nextRow = table.insertRow(-1);
    for(var j=0;j<NUMTBLCOLS;j++){
      //make a new cell
      var newCell = nextRow.insertCell(-1);
      if(j==0){ // only make a link on a first column
        var content = locations[locationsIdx[i]][j];
        var a = document.createElement('a');
        var linkText = document.createTextNode(content);
        var itemInfo = locations[locationsIdx[i]][6];
        a.appendChild(linkText);
        a.href = 'individualitempage.php?itemNum='+itemInfo;
        newCell.appendChild(a);
      }else{
        newCell.innerHTML = locations[locationsIdx[i]][j];
      }
    }   // end for
  }   // end for
    //show the table attaching the table to a certain div
  pos1.appendChild(table);
}

function deletePevTable(len){
  // This function is made to remove the prior table and map.
  // Note that a page which uses this function is using Ajax to load data.
  if(len != 0){
    // Access div showResults and make a table for results.
    var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
    //Access the above table different way
    var thisTable = pos1.getElementsByTagName("TABLE");
    //Delete the previus table not to mess up the page by making several tables.
    var howManyTables = thisTable.length;
    //alert("this is sss "+howManyTables);
    if(howManyTables >1){
      pos1.removeChild(thisTable[0]);
    }
  }else{
    var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
    alert("No No result! ");
    //Access the above table different way
    var thisTable = pos1.getElementsByTagName("TABLE");
    //Remove the first previous table
    pos1.removeChild(thisTable[0]);
    //Remove the prior google map
    var googleMap = document.getElementById("googleMap1");
    pos1.removeChild(googleMap);
  }
} // end function

function drawMap(firstLati,firstLongti,locations){
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  //Make Div element and attach it to pos1
  var googleMap = document.createElement("div");
  googleMap.setAttribute("id", "googleMap1"); // set ID
  pos1.appendChild(googleMap);
  //create a map
	var map = new google.maps.Map(document.getElementById('googleMap1'),
    {
      zoom: 12,
      center: new google.maps.LatLng(firstLati, firstLongti),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  var marker;
  var numPins=0;
  if(locations.length <= 2){
    numPins = locations.length;
  }else{
    numPins = 3;
  }
  for(var i=0;i<numPins;i++){
    marker = new google.maps.Marker({
          position:new google.maps.LatLng(locations[i][4],locations[i][5]),
                                          animation: google.maps.Animation.BOUNCE,map:map
        }); // add marker on map
    var link= '<a href="individualitempage.php?itemNum='+locations[i][6]+'">'+locations[i][0]+'</a>';
    var infowindow = new google.maps.InfoWindow({ content:link });
    infowindow.open(map,marker);
  } // end for
  marker.setMap(map);
}

function getSuburbs(suburb){
  //alert("this is "+suburb);
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  table.setAttribute("id","table1");
  //Make a table including the results
  var locations=[[],[]];
  locations = makeTable(suburb,1);
  //Access the above table different way
  var thisTable = pos1.getElementsByTagName("TABLE");
  //Delete the previus table not to mess up the page by making several tables.
  deletePevTable(locations.length);
  // Resize map in div id='googleMap1'
  var sizeDiv = document.getElementById('googleMap1');
  sizeDiv.style = "width:500px;height:400px";
  //show the maps.. Get data from DB by using Ajax
  if(locations.length==0){
    alert("No results");
  }else{
    drawMap(locations[0][4],locations[0][5],locations);
  }
} // end function getSuburbs

function searchByName(){
  //Access input data in search-text
  var result = document.getElementById('typeSubName').value;
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  //Make a table including the results
  var locations=[[],[]];
  locations = makeTable(result,0);

  //show the maps.. Get data from DB by using Ajax
  if(locations.length==0){
    deletePevTable(locations.length);
  }else{
    deletePevTable(locations.length);
    // Resize map in div id='googleMap1'
    var sizeDiv = document.getElementById('googleMap1'); //style="width:400px;height:300px;"
    sizeDiv.style = "width:500px;height:400px";
    drawMap(locations[0][4],locations[0][5],locations);
  }
} // end of function searchByName

function searchByRating(rating){
  //alert("this is "+rating);
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  //Make a table including the results
  var locations=[[],[]];
  locations = makeTable(rating,2);
  //Delete the previous table
  deletePevTable(locations.length);
  // Resize map in div id='googleMap1'
  var sizeDiv = document.getElementById('googleMap1'); //style="width:400px;height:300px;"
  sizeDiv.style = "width:500px;height:400px";
  //show the maps.. Get data from DB by using Ajax
  if(locations.length==0){
    alert("No results");
  }else{
    drawMap(locations[0][4],locations[0][5],locations);
  }
}

function nearestPlace(){
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition, showError);
  } else {
      alert("Geolocation is not supported by this browser.");
  }
}

function showPosition(position) {
  // Get the location of an user.
  var userLatitude = position.coords.latitude;
  var userLongitude = position.coords.longitude;
  var MaximumPins = 5;
  //Use built-in function to calculate the distance
  lineLength = function(x, y, x0, y0){
    return Math.sqrt((x -= x0) * x + (y -= y0) * y);
  };
  //Get data from DB by using Ajax
  var locations=[[],[]];
  locations = getDataFromDB("allResults",4);
  // Save distances into Arrays.
  var distanceArr = [];
  var distanceArr_backup = []; // required to get indexes comparing this with distanceArr
  var locationsIdx = [];
  //Insert distances into above arrays.
  for(var i=0; i < locations.length;i++){
    distanceArr[i]= lineLength(userLatitude, userLongitude, locations[i][4], locations[i][5]);
    distanceArr_backup[i] = lineLength(userLatitude, userLongitude, locations[i][4], locations[i][5]);
  }
  // Sort distances by the nearest park-space from user's current location.
  distanceArr.sort(function(a, b){return a-b});
  // Store index of the original array distanceArr_backup to access data=set locations
  for(var i=0;i<distanceArr.length;i++){
    for(var j=0;j<distanceArr.length;j++){
      if(distanceArr[i] == distanceArr_backup[j]){
        locationsIdx[i] = j;
      } // end if
    } // end inner-for
  } // end  outer-for
  // Access div showResults and make a table for results.
  var pos1 = document.getElementById("contents").getElementsByClassName("showResults")[0];
  var table = document.createElement("TABLE");
  //Make a table including the results.
    //makeTable(null,-1);
  makeTable2(locations,locationsIdx)
  //Access the above table different way
  var thisTable = pos1.getElementsByTagName("TABLE");
  //Delete the previus table not to mess up the page by making several tables.
  deletePevTable(locationsIdx.length);
  //Resize map in div id='googleMap1'
  var sizeDiv = document.getElementById('googleMap1');
  sizeDiv.style = "width:500px;height:400px";
  //show the maps
  if(locationsIdx.length==0){
    alert("No results");
  }else{
    // Draw google map
    var map = new google.maps.Map(document.getElementById('googleMap1'),
              {
                zoom: 16,
                center: new google.maps.LatLng(userLatitude, userLongitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
    var marker;
    // user's location and mark user's current position on that
    marker = new google.maps.Marker({position:new google.maps.LatLng(userLatitude,userLongitude),map:map});
    var infowindow = new google.maps.InfoWindow({content:"My current location"});
    infowindow.open(map,marker);
    //Drop pins of several available park-spaces
    for(var i=0;i<MaximumPins;i++){
    	marker = new google.maps.Marker({ position:new google.maps.LatLng(locations[locationsIdx[i]][4],locations[locationsIdx[i]][5]),
                                        animation: google.maps.Animation.BOUNCE,map:map }); // add marker on map
      var link= '<a href="individualitempage.php?itemNum='+locations[locationsIdx[i]][6]+'">'+locations[locationsIdx[i]][0]+'</a>';
      var infowindow = new google.maps.InfoWindow( { content:link });
      infowindow.open(map,marker);
    } // end for
    marker.setMap(map);
  } // end else
} // end funciton

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }
}

function sendIntoDB(parkID,userID){

  var reviewContents = document.getElementById('reviewContents').value;
  var userRating = document.getElementById('userRating').value;
  alert("userID: "+userID+" userRating: "+userRating+" contents: "+reviewContents+" parkID: "+parkID);
  if(reviewContents!="" && userRating!=""){
    //alert("say");
    //getDataFromDB()
    $.ajax({
     url:'pages/updateReviewTableByAjax.php',
     type:'post',
     dataType:'json',
     data: {parkID:parkID,userID:userID,reviewContents:reviewContents,userRating:userRating},
     async: false,
     success:function(data){
       //locations = data;
       //alert("The number of rows is "+locations.length);
     }
   });
  }

}
